# TSStreamingKitManager

Provides a wrapper around the pod StreamKit, giving easy access to playing audio files


# Demo project

Clone the project and then run pod install and use the workspace to run the code.


# Pod Install

```
pod 'TSStreamingKitManager', :git => 'https://rmoult@bitbucket.org/rmoult/tsstreamingkitmanager'
```

# Example 

```
#import "TSStreamKitManager.h"

@property (nonatomic, strong) TSStreamKitManager *streamKitManager;

self.streamKitManager = [[TSStreamKitManager alloc] init];

self.streamKitManager.delegate = self;

NSURL *URL = [NSURL URLWithString:@"http://www.terrillthompson.com/music/audio/smallf.mxx"];

[self.streamKitManager playWithURL:URL];
```