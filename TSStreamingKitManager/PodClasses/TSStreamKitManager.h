//
//  Created by Richard Moult on 15/06/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StreamingKit/STKAudioPlayer.h>


#define kStreamKitManagerLocStringCodecErrorKey         @"StreamKitManager.error.codec"
#define kStreamKitManagerLocStringGeneralErrorKey       @"StreamKitManager.error.general"
#define kStreamKitManagerLocStringNoDataErrorKey        @"StreamKitManager.error.noData"
#define kStreamKitManagerLocStringDataSourceErrorKey    @"StreamKitManager.error.dataSource"


@class TSStreamKitManager;


@protocol TSStreamKitManagerDelegate <NSObject>
- (void)streamKitManager:(TSStreamKitManager *)manager
         playingDuration:(CGFloat)playingDuration
             maxDuration:(CGFloat)maxDuration;
- (void)streamKitManager:(TSStreamKitManager *)manager isBuffering:(BOOL)buffering;
- (void)streamKitManagerDidReachEndOfTrack:(TSStreamKitManager *)manager reason:(STKAudioPlayerStopReason)reason;
- (void)streamKitManager:(TSStreamKitManager *)manager error:(NSError *)error;
@end



@interface TSStreamKitManager : NSObject

@property (nonatomic, weak) id<TSStreamKitManagerDelegate> delegate;

- (void)playWithURL:(NSURL *)URL;
- (void)seekToTime:(CGFloat)value;
- (void)pause;
- (void)resume;
- (void)stop;
- (STKAudioPlayerState)playerState;

@end
