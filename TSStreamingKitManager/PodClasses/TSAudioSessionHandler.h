//
//  Created by Richard Moult on 12/07/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>


@class TSAudioSessionHandler;


@protocol TSAudioSessionHandlerDelegate <NSObject>
// possible reason AVAudioSessionInterruptionTypeBegan
- (void)audioSessionHandler:(TSAudioSessionHandler *)audioSessionHandler interrupted:(NSInteger)reason;
// possible reason AVAudioSessionRouteChangeReasonNewDeviceAvailable
- (void)audioSessionHandler:(TSAudioSessionHandler *)audioSessionHandler routeChange:(NSInteger)reason;
@end



@interface TSAudioSessionHandler : NSObject

@property (nonatomic, weak) id<TSAudioSessionHandlerDelegate> delegate;

- (void)setUpForBackgroundAudioStreaming;

@end
