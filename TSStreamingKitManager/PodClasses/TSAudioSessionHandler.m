//
//  Created by Richard Moult on 12/07/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import "TSAudioSessionHandler.h"
#import <AVFoundation/AVFoundation.h>


@implementation TSAudioSessionHandler



- (id)init {
    
    self = [super init];
    
    if (self) {
        
        [self addNotifications];
    }
    return self;
}


- (void)setUpForBackgroundAudioStreaming {
    
    AVAudioSession * session = [AVAudioSession sharedInstance];
    
    NSError * setCategoryError = nil;
    
    if (![session setCategory:AVAudioSessionCategoryPlayback
                        error:&setCategoryError]) {
        NSLog(@"error %@", setCategoryError.localizedDescription);
    }
    
    NSError * error;
    Float32 bufferLength = 0.1;
    [session setPreferredIOBufferDuration:bufferLength error:&error];
    
    if (error) {
        NSLog(@"preferred buffer %@", error.localizedDescription);
    }
}




#pragma mark - notifications

- (void)addNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(interruption:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(routeChange:)
                                                 name:AVAudioSessionRouteChangeNotification
                                               object:nil];
}



- (void)interruption:(NSNotification*)notification {
    
    // get the user info dictionary
    NSDictionary *interuptionDict = notification.userInfo;
    // get the AVAudioSessionInterruptionTypeKey enum from the dictionary
    NSInteger interuptionType = [[interuptionDict valueForKey:AVAudioSessionInterruptionTypeKey] integerValue];
    // decide what to do based on interruption type here...
    switch (interuptionType) {
        case AVAudioSessionInterruptionTypeBegan:
            NSLog(@"Audio Session Interruption case started.");
            // fork to handling method here...
            // EG:[self handleInterruptionStarted];
            break;
            
        case AVAudioSessionInterruptionTypeEnded:
            NSLog(@"Audio Session Interruption case ended.");
            // fork to handling method here...
            // EG:[self handleInterruptionEnded];
            break;
            
        default:
            NSLog(@"Audio Session Interruption Notification case default.");
            break;
    }
    
    [self.delegate audioSessionHandler:self interrupted:interuptionType];
}


- (void)routeChange:(NSNotification*)notification {
    
    NSDictionary *interuptionDict = notification.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
        case AVAudioSessionRouteChangeReasonUnknown:
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonUnknown");
            break;
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            // a headset was added or removed
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonNewDeviceAvailable");
            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
            // a headset was added or removed
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonOldDeviceUnavailable");
            break;
            
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonCategoryChange");//AVAudioSessionRouteChangeReasonCategoryChange
            break;
            
        case AVAudioSessionRouteChangeReasonOverride:
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonOverride");
            break;
            
        case AVAudioSessionRouteChangeReasonWakeFromSleep:
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonWakeFromSleep");
            break;
            
        case AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory:
            NSLog(@"routeChangeReason : AVAudioSessionRouteChangeReasonNoSuitableRouteForCategory");
            break;
            
        default:
            break;
    }
    
    [self.delegate audioSessionHandler:self routeChange:routeChangeReason];
}



@end
