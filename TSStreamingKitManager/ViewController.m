//
//  ViewController.m
//  TSStreamingKitManager
//
//  Created by Richard Moult on 26/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import "ViewController.h"
#import "TSStreamKitManager.h"



@interface ViewController () <TSStreamKitManagerDelegate>
@property (nonatomic, strong) TSStreamKitManager *streamKitManager;
@end



@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.streamKitManager = [[TSStreamKitManager alloc] init];
    
    self.streamKitManager.delegate = self;
}


- (IBAction)playButtonPressed:(id)sender {
    
    NSURL *URL = [NSURL URLWithString:@"http://www.terrillthompson.com/music/audio/smallf.mxx"];
    
    [self.streamKitManager playWithURL:URL];
}


- (IBAction)stopButtonPressed:(id)sender {
    
    [self.streamKitManager stop];
}


#pragma mark - stream manager delegate

- (void)streamKitManager:(TSStreamKitManager *)manager
         playingDuration:(CGFloat)playingDuration
             maxDuration:(CGFloat)maxDuration {
    
    NSLog(@"playingDuration %f  maxDuration %f", playingDuration, maxDuration);
}


- (void)streamKitManager:(TSStreamKitManager *)manager isBuffering:(BOOL)buffering {
    
    NSLog(@"isBuffering %d", buffering);
}


- (void)streamKitManagerDidReachEndOfTrack:(TSStreamKitManager *)manager reason:(STKAudioPlayerStopReason)reason {
    
    NSLog(@"end of track reason %d", reason);
}


- (void)streamKitManager:(TSStreamKitManager *)manager error:(NSError *)error {
   
    NSLog(@"stream error %@", error);
}


@end
